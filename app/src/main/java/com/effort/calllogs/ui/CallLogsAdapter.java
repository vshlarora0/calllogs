package com.effort.calllogs.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.effort.calllogs.R;
import com.effort.calllogs.data.PermanentCallLog;
import com.effort.calllogs.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

public class CallLogsAdapter extends RecyclerView.Adapter<CallLogsAdapter.LogViewHolder> {

    private List<PermanentCallLog> permanentCallLogList;

    public CallLogsAdapter() {
        this.permanentCallLogList = new ArrayList<>();
    }

    public void setPermanentCallLogList(List<PermanentCallLog> permanentCallLogList) {
        this.permanentCallLogList.clear();
        this.permanentCallLogList.addAll(permanentCallLogList);
    }

    @NonNull
    @Override
    public LogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        return new LogViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_call_log, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LogViewHolder logViewHolder, int position) {
        logViewHolder.bind(permanentCallLogList.get(position));
    }

    @Override
    public int getItemCount() {
        return permanentCallLogList.size();
    }

    static class LogViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCallerNumber;
        private TextView tvCallDuration;
        private TextView tvContact;

        public LogViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCallerNumber = itemView.findViewById(R.id.tv_caller_number);
            tvCallDuration = itemView.findViewById(R.id.tv_call_duration);
            tvContact = itemView.findViewById(R.id.tv_is_contact);
        }

        public void bind(PermanentCallLog permanentCallLog) {
            tvCallerNumber.setText(permanentCallLog.getNumber());
            tvCallDuration.setText(TimeUtils.getDurationPretty(permanentCallLog.getCallEndTimestamp() -
                    permanentCallLog.getCallPickTimestamp()));
            tvContact.setText(permanentCallLog.isPresentInContactList() ?
                    "Present in contacts" : "Not in contacts");
        }
    }
}
