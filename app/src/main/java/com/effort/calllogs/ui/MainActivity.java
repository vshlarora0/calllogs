package com.effort.calllogs.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.effort.calllogs.R;
import com.effort.calllogs.data.PermanentCallLog;
import com.effort.calllogs.lifecycle.CallLogsViewModel;
import com.effort.calllogs.util.PermissionUtils;
import com.effort.calllogs.util.ViewUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int RC_APP_PERMISSIONS = 100;
    private List<String> permissionsToSeek = new ArrayList<>();

    private RecyclerView rvCallLogs;
    private CallLogsAdapter callLogsAdapter;
    private TextView tvonBoarding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        subscribeCallLogs();
    }

    private void subscribeCallLogs() {
        CallLogsViewModel callLogsViewModel = ViewModelProviders.of(this).get(CallLogsViewModel.class);
        callLogsViewModel.getCallLogs().observe(this, new Observer<List<PermanentCallLog>>() {
            @Override
            public void onChanged(@Nullable List<PermanentCallLog> permanentCallLogs) {
                if (permanentCallLogs == null || permanentCallLogs.isEmpty()) {
                    ViewUtils.setGone(rvCallLogs);
                    ViewUtils.setVisible(tvonBoarding);
                    return;
                }

                callLogsAdapter.setPermanentCallLogList(permanentCallLogs);
                callLogsAdapter.notifyDataSetChanged();

                ViewUtils.setVisible(rvCallLogs);
                ViewUtils.setGone(tvonBoarding);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }
    }

    private void initViews() {
        rvCallLogs = findViewById(R.id.rv_call_logs);
        tvonBoarding = findViewById(R.id.tv_onboarding);

        callLogsAdapter = new CallLogsAdapter();
        rvCallLogs.setAdapter(callLogsAdapter);
        rvCallLogs.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        rvCallLogs.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkAndRequestPermissions() {

        // check phone state permission
        if (!PermissionUtils.isPermissionGranted(this, Manifest.permission.READ_PHONE_STATE)) {
            permissionsToSeek.add(Manifest.permission.READ_PHONE_STATE);
        }

        // check read contacts permission
        if (!PermissionUtils.isPermissionGranted(this, Manifest.permission.READ_CONTACTS)) {
            permissionsToSeek.add(Manifest.permission.READ_CONTACTS);
        }

        boolean canSeekPermissions = true;

        if (permissionsToSeek.size() > 0) {
            // check if should show permission rationale for any of the sought permissions

            for (String permission : permissionsToSeek) {
                if (shouldShowRequestPermissionRationale(permission)) {
                    PermissionUtils.showRequestPermissionRationale(this,
                            "Please grant permission to " + PermissionUtils.getPermissionDescription(permission) +
                                    " for the app to function smoothly.");
                    canSeekPermissions = false;
                    break;
                }
            }

            if (canSeekPermissions) {
                String[] permissions = new String[permissionsToSeek.size()];
                permissionsToSeek.toArray(permissions);
                requestPermissions(permissions, RC_APP_PERMISSIONS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RC_APP_PERMISSIONS) {
            // check if permissions have been granted
            for (int i = 0; i < grantResults.length; i++) {
                if (PackageManager.PERMISSION_GRANTED == grantResults[i]) {
                    permissionsToSeek.remove(permissions[i]);
                }
            }
        }
        checkAndRequestPermissions();
    }
}
