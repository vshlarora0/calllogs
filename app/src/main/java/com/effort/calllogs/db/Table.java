package com.effort.calllogs.db;

public class Table {

    public static final class Logs {

        public static final String TABLE_NAME = "Logs";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                Column.NUMBER + " INTEGER, " +
                Column.CALL_PICK_TIMESTAMP + " INTEGER ," +
                Column.CALL_END_TIMESTAMP + " INTEGER, " +
                Column.TIMESTAMP + " INTEGER " + ")";

        public static final class Column {
            public static final String NUMBER = "number";
            public static final String CALL_PICK_TIMESTAMP = "call_pick_timestamp";
            public static final String CALL_END_TIMESTAMP = "call_end_timestamp";
            public static final String TIMESTAMP = "added_on";
        }
    }

    public static final class TempLogs {

        public static final String TABLE_NAME = "TempLogs";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                Column.NUMBER + " INTEGER, " +
                Column.CALL_PICK_TIMESTAMP + " INTEGER " + ")";

        public static final class Column {
            public static final String NUMBER = "number";
            public static final String CALL_PICK_TIMESTAMP = "call_pick_timestamp";
        }
    }
}
