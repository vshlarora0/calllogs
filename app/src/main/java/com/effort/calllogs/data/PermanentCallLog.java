package com.effort.calllogs.data;

public class PermanentCallLog extends CallLog {
    private final long callPickTimestamp;
    private final long callEndTimestamp;
    private transient boolean isPresentInContactList;

    public PermanentCallLog(String number, long callPickTimestamp, long callEndTimestamp) {
        super(number);
        this.callPickTimestamp = callPickTimestamp;
        this.callEndTimestamp = callEndTimestamp;
    }

    public long getCallPickTimestamp() {
        return callPickTimestamp;
    }

    public long getCallEndTimestamp() {
        return callEndTimestamp;
    }

    public boolean isPresentInContactList() {
        return isPresentInContactList;
    }

    public void setPresentInContactList(boolean presentInContactList) {
        isPresentInContactList = presentInContactList;
    }
}
