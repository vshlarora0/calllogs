package com.effort.calllogs.data;

public class CallLog {
    private final String number;

    public CallLog(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }
}
