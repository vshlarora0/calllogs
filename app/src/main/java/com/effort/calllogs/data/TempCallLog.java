package com.effort.calllogs.data;

public class TempCallLog extends CallLog {

    private final long callPickTimestamp;

    public TempCallLog(String number, long callPickTimestamp) {
        super(number);
        this.callPickTimestamp = callPickTimestamp;
    }

    public long getCallPickTimestamp() {
        return callPickTimestamp;
    }
}
