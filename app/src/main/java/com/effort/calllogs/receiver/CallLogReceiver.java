package com.effort.calllogs.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.effort.calllogs.data.PermanentCallLog;
import com.effort.calllogs.data.TempCallLog;
import com.effort.calllogs.repo.CallLogsRepository;
import com.effort.calllogs.util.Logger;

public class CallLogReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // get the call state
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        // get the caller number
        String callerNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

        Logger.log("Call State " + state + " Caller " + callerNumber);

        CallLogsRepository callLogsRepository = CallLogsRepository.getInstance(context.getApplicationContext());

        if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            // purge any entries for that phone number from the temporary table
            callLogsRepository.removeTemporaryCallLog(callerNumber);

            // and make a new entry
            callLogsRepository.insertTemporary(new TempCallLog(callerNumber, (System.currentTimeMillis())));
        } else if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)) {

            // get the entry from the temporary table
            TempCallLog tempCallLog = callLogsRepository.getTemporaryCallLog(callerNumber);

            // if no entry present in temp table that means call is declined
            if (tempCallLog == null) {
                return;
            }

            // insert entry in the truth table
            callLogsRepository.insertPermanent(new PermanentCallLog(callerNumber,
                    tempCallLog.getCallPickTimestamp(), System.currentTimeMillis()));

            // remove the entry from the temporary table
            callLogsRepository.removeTemporaryCallLog(callerNumber);
        }
    }
}
