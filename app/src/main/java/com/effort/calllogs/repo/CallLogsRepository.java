package com.effort.calllogs.repo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.effort.calllogs.data.PermanentCallLog;
import com.effort.calllogs.data.TempCallLog;
import com.effort.calllogs.db.DbHelper;

import java.util.List;

public class CallLogsRepository {

    private static CallLogsRepository repository;

    private CallLogDao callLogDao;
    private MutableLiveData<List<PermanentCallLog>> callLogsLiveData;

    private CallLogsRepository(Context applicationContext) {
        this.callLogDao = new CallLogDao(DbHelper.getInstance(applicationContext));
        this.callLogsLiveData = new MutableLiveData<>();
        this.callLogsLiveData.setValue(getPermanentCallLogs());
    }

    public static CallLogsRepository getInstance(Context applicationContext) {
        if (repository == null) {
            synchronized (CallLogsRepository.class) {
                if (repository == null) {
                    repository = new CallLogsRepository(applicationContext);
                }
            }
        }
        return repository;
    }

    public LiveData<List<PermanentCallLog>> getCallLogs() {
        return callLogsLiveData;
    }

    public void insertTemporary(TempCallLog callLog) {
        callLogDao.insertTemporary(callLog);
    }

    public void insertPermanent(PermanentCallLog callLog) {
        callLogDao.insertPermanent(callLog);
        callLogsLiveData.setValue(getPermanentCallLogs());
    }

    public TempCallLog getTemporaryCallLog(String callerNumber) {
        return callLogDao.getTemporaryCallLog(callerNumber);
    }

    public void removeTemporaryCallLog(String callerNumber) {
        callLogDao.removeTemporaryCallLog(callerNumber);
    }

    public List<PermanentCallLog> getPermanentCallLogs() {
        return callLogDao.getPermanentCallLogs();
    }
}
