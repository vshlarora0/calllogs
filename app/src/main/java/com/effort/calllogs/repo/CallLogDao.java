package com.effort.calllogs.repo;

import android.content.ContentValues;
import android.database.Cursor;

import com.effort.calllogs.data.PermanentCallLog;
import com.effort.calllogs.data.TempCallLog;
import com.effort.calllogs.db.DbHelper;
import com.effort.calllogs.db.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Call Log Data Access Object
 * This class is responsible for all CRUD operations related to Call Log Data Object
 */
class CallLogDao {

    private DbHelper dbHelper;

    CallLogDao(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    void insertTemporary(TempCallLog callLog) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Table.TempLogs.Column.NUMBER, callLog.getNumber());
        contentValues.put(Table.TempLogs.Column.CALL_PICK_TIMESTAMP, callLog.getCallPickTimestamp());
        dbHelper.getWritableDatabase().insert(Table.TempLogs.TABLE_NAME, null, contentValues);
    }

    void insertPermanent(PermanentCallLog callLog) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Table.Logs.Column.NUMBER, callLog.getNumber());
        contentValues.put(Table.Logs.Column.CALL_PICK_TIMESTAMP, callLog.getCallPickTimestamp());
        contentValues.put(Table.Logs.Column.CALL_END_TIMESTAMP, callLog.getCallEndTimestamp());
        contentValues.put(Table.Logs.Column.TIMESTAMP, System.currentTimeMillis());
        dbHelper.getWritableDatabase().insert(Table.Logs.TABLE_NAME, null, contentValues);
    }

    TempCallLog getTemporaryCallLog(String callerNumber) {
        Cursor cursor = dbHelper.getReadableDatabase().query(Table.TempLogs.TABLE_NAME,
                null, Table.TempLogs.Column.NUMBER + "= ? ",
                new String[]{callerNumber}, null, null, null);

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        String callPickTimeStamp = cursor.getString(cursor.getColumnIndex(Table.TempLogs.Column.CALL_PICK_TIMESTAMP));
        cursor.close();

        return new TempCallLog(callerNumber, Long.valueOf(callPickTimeStamp));
    }

    void removeTemporaryCallLog(String callerNumber) {
        dbHelper.getWritableDatabase().delete(Table.TempLogs.TABLE_NAME,
                Table.TempLogs.Column.NUMBER + "= ?",
                new String[]{callerNumber});
    }

    List<PermanentCallLog> getPermanentCallLogs() {
        Cursor cursor = dbHelper.getReadableDatabase().query(Table.Logs.TABLE_NAME,
                null, null, null, null, null,
                Table.Logs.Column.TIMESTAMP + " DESC");

        List<PermanentCallLog> permanentCallLogs = new ArrayList<>(cursor.getCount());

        while (cursor.moveToNext()) {
            String callNumber = cursor.getString(cursor.getColumnIndex(Table.Logs.Column.NUMBER));
            String callPickTimestamp = cursor.getString(cursor.getColumnIndex(Table.Logs.Column.CALL_PICK_TIMESTAMP));
            String callEndTimestamp = cursor.getString(cursor.getColumnIndex(Table.Logs.Column.CALL_END_TIMESTAMP));
            permanentCallLogs.add(new PermanentCallLog(callNumber, Long.valueOf(callPickTimestamp), Long.valueOf(callEndTimestamp)));
        }

        cursor.close();

        return permanentCallLogs;
    }
}
