package com.effort.calllogs.util;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

public class PermissionUtils {


    public static void showRequestPermissionRationale(final Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Enable permission");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);

                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static String getPermissionDescription(String permission) {
        switch (permission) {
            case Manifest.permission.READ_PHONE_STATE:
                return "Read Phone State";
            case Manifest.permission.READ_CONTACTS:
                return "Read Contacts";
            default:
                return "";
        }
    }
}
