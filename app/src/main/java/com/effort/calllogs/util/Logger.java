package com.effort.calllogs.util;

import android.util.Log;

public class Logger {

    public static final String TAG = Logger.class.getSimpleName();

    public static void log(String message) {
        Log.d(TAG, message);
    }
}
