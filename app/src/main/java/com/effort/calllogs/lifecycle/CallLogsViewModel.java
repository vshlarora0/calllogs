package com.effort.calllogs.lifecycle;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.effort.calllogs.data.PermanentCallLog;
import com.effort.calllogs.repo.CallLogsRepository;

import java.util.List;

public class CallLogsViewModel extends AndroidViewModel {

    private CallLogsRepository callLogsRepository;

    private MediatorLiveData<List<PermanentCallLog>> callLogsLiveData;

    public CallLogsViewModel(@NonNull final Application application) {
        super(application);
        callLogsLiveData = new MediatorLiveData<>();
        callLogsRepository = CallLogsRepository.getInstance(application);
        callLogsLiveData.addSource(callLogsRepository.getCallLogs(), new Observer<List<PermanentCallLog>>() {
            @Override
            public void onChanged(@Nullable List<PermanentCallLog> permanentCallLogs) {
                for (PermanentCallLog callLog : permanentCallLogs) {
                    if (contactExists(application, callLog.getNumber())) {
                        callLog.setPresentInContactList(true);
                    }
                }
                callLogsLiveData.setValue(permanentCallLogs);
            }
        });
    }

    public LiveData<List<PermanentCallLog>> getCallLogs() {
        return callLogsLiveData;
    }

    private boolean contactExists(Context context, String number) {
        Uri lookupUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup.NUMBER};
        Cursor cursor = context.getContentResolver().query(lookupUri, mPhoneNumberProjection,
                null, null, null);
        try {
            return cursor.getCount() > 0;
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }
}
